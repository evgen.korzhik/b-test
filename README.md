The requirement about generation of 100.000 raw data row can not be implemented.
If we have 100 unique user_ids and 100 unique event_ids, maximum number of unique pairs is 1.000

### Run:
```
docker-compose up -d b-test-postgres
docker-compose up generate-data
docker-compose up agg_data
docker-compose up check-agg
```
check_agg raises exception if calculated data is wrong